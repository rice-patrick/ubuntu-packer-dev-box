#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive
sudo su - root

# Install create a temp dir for download files
mkdir /tmp/chrome
cd /tmp/chrome

apt-get update
apt --fix-broken -qq -y install 

# Download Chrome
wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt-get install ./google-chrome-stable_current_amd64.deb

# Cleanup temp directory
cd /tmp
rm -rf /tmp/chrome
