#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive
sudo su - root

# force accept config options
echo "Dpkg::Options {
  \"--force-confdef\";
  \"--force-confold\";
}" >> /etc/apt/apt.conf.d/local

# Install ubuntu unity
apt-get autoremove -y
apt-get upgrade -y
apt-get install -y xinit ubuntu-desktop

# Disable start menu animations
gsettings set org.gnome.desktop.interface enable-animations false

#Disable the "welcome" screen in gnome
mkdir -p /home/vagrant/.config
echo "yes" >> /home/vagrant/.config/gnome-initial-setup-done

# Fix permissions on that vagrant folder now.
chown -R vagrant:vagrant /home/vagrant
