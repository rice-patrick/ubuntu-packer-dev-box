#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive
sudo su - root

apt-get install unzip
mkdir -p /tmp/terraform
cd /tmp/terraform

# Install a recent version of tf
wget -q https://releases.hashicorp.com/terraform/1.3.9/terraform_1.3.9_linux_amd64.zip
unzip -qq -o terraform_1.3.9_linux_amd64.zip
mv terraform /usr/local/bin/terraform
