#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# install git
apt-get install -qq -y git-core

# install git-cola, as gitkraken not avaiable for arm
apt-get install -qq -y git-cola