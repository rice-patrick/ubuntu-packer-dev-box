#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# Add vagrant user to sudoers.
# echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

# Disable daily apt unattended updates.
echo 'APT::Periodic::Enable "0";' >> /etc/apt/apt.conf.d/10periodic

# Import the base vagrant key (it will generate a new one on first startup)
mkdir /home/vagrant/.ssh
wget --no-check-certificate -q -O authorized_keys 'https://github.com/mitchellh/vagrant/raw/master/keys/vagrant.pub'
mv authorized_keys /home/vagrant/.ssh
chown -R vagrant /home/vagrant/.ssh
chmod -R go-rwsx /home/vagrant/.ssh

# update source list to make ubuntu-desktop able to be installed
apt update
apt-get install -y tasksel
