#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# force accept config options
echo "Dpkg::Options {
  \"--force-confdef\";
  \"--force-confold\";
}" >> /etc/apt/apt.conf.d/local

# Install ubuntu unity
apt-get autoremove -y
apt-get upgrade -y
apt install -y ubuntu-desktop

#Disable the "welcome" screen in gnome - Throwing a bunch of stuff at the wall here
# Since gnome isn't great at obeying settings
mkdir -p /home/vagrant/.config
echo "yes" >> /home/vagrant/.config/gnome-initial-config-done
apt-get remove -y --auto-remove gnome-initial-setup


# Fix permissions on that vagrant folder now.
chown -R vagrant:vagrant /home/vagrant

