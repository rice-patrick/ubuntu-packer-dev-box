#!/bin/bash -eux

export DEBIAN_FRONTEND=noninteractive

# Change to tmp to prevent issues from executing commands in a non-existance folder
cd /tmp

# install ansible
apt-get update -qq -y
apt-get install -qq -y software-properties-common
apt-add-repository --yes --update ppa:ansible/ansible
apt-get install -qq -y ansible

# install libnss3 (used in provisioner scripts to install SSL certificates)
apt install -y libnss3-tools

# Cleanup terraform install
rm -rf /tmp/terraform

# Install awscli
cd /tmp
curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

# Install gradle
apt-get install -qq -y gradle

# Install NodeJS & NPM
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
chmod +x ./nodesource_setup.sh
./nodesource_setup.sh
apt-get install -y nodejs build-essential

# Install glci (run gitlab pipelines locally)
npm install -g yarn
yarn global add glci
# install docker
apt-get install -qq -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# install docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod 0755 /usr/local/bin/docker-compose

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-get update -qq -y
apt install -qq -y docker.io

# Add the vagrant user to the "docker" group so they can run commands without sudo
usermod -a -G docker vagrant

# Install Postman
# Postman does not support ARM64 yet: https://github.com/postmanlabs/postman-app-support/issues/5816
# mkdir /tmp/postman
# cd /tmp/postman

# Install libconf in case it isn't installed
apt install -qq -y libgconf-2-4

# wget -q https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
# mkdir -p /home/vagrant/
# tar -xzf postman.tar.gz -C /home/vagrant/
# ln -s /home/vagrant/Postman/Postman /usr/bin/postman
# rm postman.tar.gz

# chown -R vagrant/vagrant /home/vagrant/Postman

# For whatever reason, postman's install oscillates between having the icon in app/resources/app and just having it in resources/app. So we need to find it first.
# export ICON_LOC=$(find /home/vagrant/Postman -name icon.png)

# echo "[Desktop Entry]
# Type=Application
# Name=Postman
# Comment=Spring Tool Suite
# Icon=${ICON_LOC}
# Exec=postman
# Terminal=false
# Categories=Development;IDE;Java;" > /usr/share/applications/postman.desktop

# Install Gauge
mkdir /tmp/gauge
cd /tmp/gauge

wget -q -O gauge.zip https://github.com/getgauge/gauge/releases/download/v1.4.1/gauge-1.4.1-linux.arm64.zip
unzip ./gauge.zip -d /usr/bin/
chown vagrant/vagrant /usr/bin/gauge

cd /tmp
rm -rf /tmp/gauge

# Remove a bunch of unnecessary favorites from the sidebar
# sudo -Hu vagrant dbus-launch gsettings set org.gnome.shell favorite-apps "['firefox.desktop']"
