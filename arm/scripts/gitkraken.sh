#!/bin/bash -eux
export DEBIAN_FRONTEND=noninteractive

# Install gitkraken
mkdir -p /tmp/gitkraken
cd /tmp/gitkraken

# TODO: GitKraken does not support ARM right now, so some other Git UI Will be required.

# Cleanup gitkraken install
cd /tmp
rm -rf /tmp/gitkraken
