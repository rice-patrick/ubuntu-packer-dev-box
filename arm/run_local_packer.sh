#!/bin/bash

# Install packer and vagrant
brew tap hashicorp/tap
brew install hashicorp/tap/packer
brew install vagrant

# Install qemu
brew install qemu
brew install knazarov/qemu-virgl/qemu-virgl

# run the packer build
packer build ./ubuntu.json -only=qemu

