Box Development and Contributions
=================================

Building the Box
----------------
`cd amd`
`packer build ubuntu.json`

Testing the Box
---------------
Add box to vagrant: `vagrant box add my-box testbox_virtualbox.box`

In your project update the Vagrantfile to point to the test box: `config.vm.box = "my-box"` 

Contributing
------------
Create a merge request to rice-patrick/ubuntu-packer-dev-box